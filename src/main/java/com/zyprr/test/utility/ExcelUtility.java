package com.zyprr.test.utility;

import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtility {
	private XSSFSheet xcelWSheet;
	private XSSFWorkbook xcelWBook;

	// Constructor to connect to the Excel with sheetname and Path
	public ExcelUtility(String path, String SheetName) throws Exception {
		InputStream instream = null;
		try {
			// Open the Excel file
//			System.out.println("path=" +Path);
		System.out.println("sname=" +SheetName);
			if (!path.startsWith("/")){
				path = "/" + path;
			}
			instream = getClass().getResourceAsStream(path);
			//FileInputStream testDataFile = new FileInputStream(Path);

			// Access the required test data sheet
			xcelWBook = new XSSFWorkbook(instream);
			xcelWSheet = xcelWBook.getSheet(SheetName);
		} catch (Exception e) {
			throw (e);
		} finally {
			if (instream != null){
				try {instream.close();}catch (Exception e){}
			}
		}
	}

	public int getNumberOfRows() throws Exception {

		try {
			System.out.println("xcel=" + xcelWSheet);
			System.out.println("rows=" + xcelWSheet.getPhysicalNumberOfRows());
			return xcelWSheet.getPhysicalNumberOfRows();
		} catch (Exception e) {
			throw (e);
		}
	}

	public String getCellDataasstring(int RowNum, int ColNum) throws Exception {
		String CellData = "";

		try {
			 CellData = xcelWSheet.getRow(RowNum).getCell(ColNum).getStringCellValue();
			 //System.out.println("The value of CellData: " + CellData);
		} catch (Exception e) {
		}
		return CellData;

		
	}


	public double getCellDataasnumber(int RowNum, int ColNum) throws Exception {

		try {
			double CellData = xcelWSheet.getRow(RowNum).getCell(ColNum).getNumericCellValue();
			// System.out.println("The value of CellData " + CellData);
			return CellData;
		} catch (Exception e) {
			return 000.00;
		}
	}
}
