package com.zyprr.test.base.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class BasePhoneyPopupPageObject extends BasePopupPageObject{

	protected BasePhoneyPopupPageObject(WebDriver webDriver) {
		super(webDriver);
	}
	
	abstract public WebElement getParentElement();

}
