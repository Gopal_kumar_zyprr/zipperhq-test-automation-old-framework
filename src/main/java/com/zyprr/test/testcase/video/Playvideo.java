package com.zyprr.test.testcase.video;

import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.zyprr.test.base.factory.ZyprrPageFactory;
import com.zyprr.test.config.Config;
import com.zyprr.test.pageobject.common.ZipperHQHome;
import com.zyprr.test.testcase.abs.AbstractTest;

public class Playvideo extends AbstractTest {

	private Map<String, Object> inputData = null;

	private static final String[] fieldNames = { };

	
	private static Editvideo staticInstance = new Editvideo(null);

	@Factory(dataProvider = "dataMethod")
	public Playvideo(Map<String, Object> inputData) {
		this.inputData = inputData;
	}

	@DataProvider
	public static Object[][] dataMethod() throws Exception {
		return staticInstance.getData();
	}

	@Test(priority = 0)
	public void PlayvideoTest() throws InterruptedException {

		ZyprrPageFactory.getInstance()
				.getPageObject(webDriver, ZipperHQHome.class)
				.xferToVideoHomePage()
				.pressPlay().pressPlayResume();
		

	}

	@Override
	protected String getDataSheetName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String[] getFieldNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getDataPath() {
		// TODO Auto-generated method stub
		return null;
	}



	

}