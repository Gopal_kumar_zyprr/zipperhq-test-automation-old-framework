package com.zyprr.test.pageobject.common ;

import java.util.List ;
import org.openqa.selenium.By ;
import org.openqa.selenium.WebDriver ;
import org.openqa.selenium.WebElement ;
import org.openqa.selenium.support.CacheLookup ;
import org.openqa.selenium.support.FindBy ;
import org.openqa.selenium.support.ui.Select ;
import com.zyprr.test.base.pageobject.BasePageObject ;

public class BaseHome extends BasePageObject {

	private static final String	xpTableRows	= "//table[@id='dataTable']/tbody/tr" ;

	protected BaseHome(
			WebDriver webDriver) {
		super(webDriver) ;
	}

	@ CacheLookup
	@ FindBy(xpath = "//div[@id='pagingControls']/div[1]/div[@name='listPrev']")
	WebElement	prevList ;

	@ CacheLookup
	@ FindBy(xpath = "//div[@id='pagingControls']/div[1]/div[@name='listNext']")
	WebElement	nextList ;

	@ CacheLookup
	@ FindBy(xpath = "//div/div/div[@id='pageText']/ul/li/a/span[@class='pageStartIndex']")
	WebElement	pageStartIndex ;

	@ CacheLookup
	@ FindBy(xpath = "//div/div/div[@id='pageText']/ul/li/a/span[@class='pageEndIndex']")
	WebElement	pageEndIndex ;

	@ CacheLookup
	@ FindBy(xpath = "//div/div/div[@id='pageText']/ul/li/a/span[@class='totalCount']")
	WebElement	totalCountIndex ;

	@ CacheLookup
	@ FindBy(xpath = "//div[@id='pageText']/ul/li/ul/li[@name='listFirst']")
	WebElement	firstList ;

	@ CacheLookup
	@ FindBy(xpath = "//div[@id='pageText']/ul/li/ul/li[@name='listLast']")
	WebElement	lastList ;

	@ CacheLookup
	@ FindBy(xpath = "//div[@id='pageText']/ul/li/a")
	WebElement	pagingCtrl ;

	@ CacheLookup
	@ FindBy(xpath = "//div[@id='pageText']/ul/li/a")
	WebElement	searchTableRows ;

	private Select getRecordListDrpDn() {
		return new Select(webDriver.findElement(By.id("recordList"))) ;
	}

	public WebElement getNextList() {
		return this.nextList ;
	}

	public BaseHome setRecordList(
			String listCnt) {
		getRecordListDrpDn().selectByVisibleText(listCnt) ;
		// Thread.sleep(10000) ;
		return this ;
	}

	public int getSearchTableRowCount() {
		List < WebElement > tableRows = webDriver.findElements(By.xpath(xpTableRows)) ;
		return tableRows.size() ;

	}
	// ****************

	public WebElement getFirstList() {
		return this.firstList ;
	}

	public WebElement getLastList() {
		return this.lastList ;
	}

	public WebElement getPreviousList() {
		return this.prevList ;
	}

//	public BaseHome setPagingCtrl() {
//		pagingCtrl.click() ;
//		return this ;
//	}

	public WebElement getStartIndex() {
		return this.pageStartIndex ;
	}

	public WebElement getEndIndex() {
		return this.pageEndIndex ;
	}

	public WebElement getTotalSearchCount() {
		return totalCountIndex ;
	}

//	@ Override
//	protected WebElement [ ] getPageLoadIndicatorElements() {
//		WebElement [ ] loadIndicatorElements = new WebElement [1] ;
	//	loadIndicatorElements [0] = searchTableRows ;
//		return loadIndicatorElements ;
	//}

	@ Override
	protected void init() {}

	@Override
	protected WebElement[] getPageLoadIndicatorElements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void waitForPageLoad() {
		// TODO Auto-generated method stub
		
	}

//	@ Override
//	public void waitForPageLoad() {
//		WebElement [ ] loadIndicatorElements = getPageLoadIndicatorElements() ;
//		WaitUtil.getInstance().waitForElementTobeVisible(webDriver,
//				loadIndicatorElements [0]) ;
//	}

}
