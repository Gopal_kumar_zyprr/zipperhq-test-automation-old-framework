package com.zyprr.test.pageobject.common;

import java.awt.datatransfer.StringSelection;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.awt.*;
import java.awt.event.KeyEvent;
import com.zyprr.test.base.factory.ZyprrPageFactory;

public class videoHome extends BaseHome {
	
	public videoHome(WebDriver webDriver) {
		super(webDriver);
	}
	
	@CacheLookup
	@FindBy(id = "videos")
	WebElement Videos;
	
	
	@CacheLookup
	@FindBy(id = "video_uploader_up_btn")
	WebElement videoUpload;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"mediaDiv\"]/div[1]/div[1]/div/div[2]/div/div[2]/div[2]/div/ul/li[1]/div[3]/div[3]/a/i")
	WebElement editVideoBtn;
	
	@CacheLookup
	@FindBy(id = "video_edit_title")
	WebElement editVideoTitle;   
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"mediaDiv\"]/div[1]/div[1]/div/div[2]/div/div[2]/div[2]/div/ul/li[1]/div[3]/div[2]/a/i")
	WebElement playVideoBtn;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"hqVideo\"]/div[1]")
	WebElement playVideo;
	
	@CacheLookup
	@FindBy(id = "hqVideo")
	WebElement playResumeBtn;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"hqVideo\"]/div[4]/div[1]/button/span[1]")
	WebElement volumeMuteBtn;
	
	@CacheLookup
	@FindBy(id = "Preview-dialog-close")
	WebElement previewDialogClose;
	
	
	
	@CacheLookup
	@FindBy(id = "video_list_left_ul_li_thisweek")
	WebElement videoListThisWeeek;
	
	
	@CacheLookup
	@FindBy(id = "video_list_left_ul_li_lastweek")
	WebElement videoListLastWeeek;
	
	
	@CacheLookup
	@FindBy(id = "video_list_left_ul_li_thismonth")
	WebElement videoListThisMonth;
	
	@CacheLookup
	@FindBy(id = "video_list_left_ul_li_lastmonth")
	WebElement videoListLastMonth;
	
	@CacheLookup
	@FindBy(id = "video_list_left_ul_li_all")
	WebElement videoListAll;
	
	@CacheLookup
	@FindBy(id = "video_tertiary-cancel-btn")
	WebElement videoDeleteCancel;
	

	@CacheLookup
	@FindBy(xpath = "//*[@id=\"mediaDiv\"]/div[1]/div[1]/div/div[2]/div/div[2]/div[2]/div/ul/li[1]/div[3]/div[4]/a/i")
	WebElement videoDeleteBtn;
	
	
	@CacheLookup
	@FindBy(id = "video_recorder_btn")
	WebElement recordVideoBtn;
	
	
	
	@CacheLookup
	@FindBy(id = "recorder_modal_play_btn")
	WebElement recordStartBtn;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recorderVMod\"]/div[7]/button[1]")
	WebElement recordResumeBtn;
	
	
	
	@CacheLookup
	@FindBy(id = "recorder_modal_save_btn")
	WebElement recordSaveBtn;
	
	
	
	@CacheLookup
	@FindBy(id = "recorder_modal_close_btn")
	WebElement recorderCloseBtn;
	
	
	
	@CacheLookup
	@FindBy(id = "recorder_modal_createcamp_btn")
	WebElement recordCreateCampaignBtn;
	
	
	

	
	
	
	public videoHome pressEdit() throws InterruptedException {
		Thread.sleep(4000);
		editVideoBtn.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				videoHome.class);
	}
	
	
	
	public videoHome editVideoTitle() throws InterruptedException {
		Thread.sleep(2000);
		editVideoTitle.clear();
		Thread.sleep(2000);
		editVideoTitle.sendKeys("Adventure");
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				videoHome.class);
	}
	
	public videoHome pressPlay() throws InterruptedException {
		Thread.sleep(4000);
		playVideoBtn.click();
		Thread.sleep(2000);
		playVideo.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				videoHome.class);
	}
	
    public videoHome pressPlayResume() throws InterruptedException {
		Thread.sleep(2000);
		playResumeBtn.click();
		Thread.sleep(2000);
		previewDialogClose.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				videoHome.class);
	}
    
    public videoHome checkCategories() throws InterruptedException {
		Thread.sleep(4000);
		videoListThisWeeek.click();
		Thread.sleep(4000);
		videoListLastWeeek.click();
		Thread.sleep(4000);
		videoListThisMonth.click();
		Thread.sleep(4000);
		videoListLastMonth.click();
		Thread.sleep(4000);
		videoListAll.click();
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				videoHome.class);
	}
    

	public videoHome delete() throws InterruptedException {
		Thread.sleep(4000);
		videoDeleteBtn.click();
		Thread.sleep(4000);
		videoDeleteCancel.click();
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				videoHome.class);
	}
	  public videoHome uploadvideo() throws InterruptedException, AWTException {
			Thread.sleep(2000);
			videoUpload.click();
			Thread.sleep(2000);
			 StringSelection ss = new StringSelection("/home/zyprr/Videos/adventure.mp4");
		        Thread.sleep(2000);
		        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
                Robot robot = new Robot();
		        robot.keyPress(KeyEvent.VK_CONTROL);
		        robot.keyPress(KeyEvent.VK_V);
		        robot.keyRelease(KeyEvent.VK_V);
		        robot.keyRelease(KeyEvent.VK_CONTROL);
		        robot.keyPress(KeyEvent.VK_ENTER);
		        robot.keyRelease(KeyEvent.VK_ENTER);
		        Thread.sleep(25000);
			return ZyprrPageFactory.getInstance().getPageObject(webDriver,
					videoHome.class);
		}
	  
	  public videoHome recordSave() throws InterruptedException {
			Thread.sleep(4000);
			recordVideoBtn.click();
			Thread.sleep(2000);
			recordStartBtn.click();
			Thread.sleep(5000);
			recordResumeBtn.click();
			Thread.sleep(7000);
			recordSaveBtn.click();
			Thread.sleep(10000);
			return ZyprrPageFactory.getInstance().getPageObject(webDriver,
					videoHome.class);
		}
	    
	  public videoHome recordCreateCampaign() throws InterruptedException {
			Thread.sleep(4000);
			recordCreateCampaignBtn.click();
			return ZyprrPageFactory.getInstance().getPageObject(webDriver,
					videoHome.class);
		}
}
