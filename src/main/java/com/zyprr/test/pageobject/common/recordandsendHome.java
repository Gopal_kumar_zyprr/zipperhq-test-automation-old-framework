

package com.zyprr.test.pageobject.common;

import java.awt.datatransfer.StringSelection;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.awt.*;
import java.awt.event.KeyEvent;
import com.zyprr.test.base.factory.ZyprrPageFactory;

public class recordandsendHome extends BaseHome {
	
	public recordandsendHome(WebDriver webDriver) {
		super(webDriver);
	}

	
	@CacheLookup
	@FindBy(id = "video_recorder_btn")
	WebElement recordVideoBtn;
	
	
	
	@CacheLookup
	@FindBy(id = "recorder_modal_play_btn")
	WebElement recordStartBtn;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recorderV\"]/div[7]/button[1]]")
	WebElement recordStopBtn;
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recorder_inline_play_overlay\"]/button")
	WebElement recordBtn;
	
	@CacheLookup
	@FindBy(id = "recsend_act_template_select")
	WebElement templateBtn;
	
	@CacheLookup
	@FindBy(id = "qs_temp_sel_thumb_url_0")
	WebElement template1;
	
	
	@CacheLookup
	@FindBy(id = "recsend_act_sub_input")
	WebElement subjectTextBox;
	
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"textQS_placeholder\"]/textarea")
	WebElement addTextBox;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"videoRecAnc_zipper\"]/videorecorder/div[1]/a/i")
	WebElement recordVideoBoxclose;
	
	

	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recordAndSendDialogHead\"]/div/div/div[2]/div[6]/div[2]/div/label")
	WebElement emailOpensNotifyCheckBox;
	
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recordAndSendDialogHead\"]/div/div/div[2]/div[6]/div[3]/div/label")
	WebElement videoPlaysNotifyCheckBox;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recordAndSendDialogHead\"]/div/div/div[2]/div[3]/div[2]/div[2]/label")
	WebElement listCheckBox;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recordAndSendDialogHead\"]/div/div/div[2]/div[3]/div[2]/div[1]/label")
	WebElement emailCheckBox;
	
	
	
	@CacheLookup
	@FindBy(id = "recsend_act_send_btn")
	WebElement sendBtn;
	
	@CacheLookup
	@FindBy(id = "recsend_act_saveexit_btn")
	WebElement saveExitBtn;
	

	@CacheLookup
	@FindBy(id = "recsend_act_email_auto")
	WebElement toTextBox;
	
	
	public recordandsendHome Textsend() throws InterruptedException {
		Thread.sleep(2000);
		recordVideoBoxclose.click();
		Thread.sleep(2000);
		templateBtn.click();
		Thread.sleep(4000);
		template1.click();
		Thread.sleep(2000);
		subjectTextBox.clear();
		subjectTextBox.sendKeys("Test Record And Send");
		Thread.sleep(2000);
		addTextBox.sendKeys("ZipperHQ makes it quick, easy and affordable to add personalized videos to your email, social and SMS communications.");
		Thread.sleep(2000);
		emailOpensNotifyCheckBox.click();
		videoPlaysNotifyCheckBox.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	public recordandsendHome toEmail() throws InterruptedException {
		Thread.sleep(2000);
		listCheckBox.click();
		emailCheckBox.click();
		Thread.sleep(2000);
		toTextBox.sendKeys("gopalkumaratwork@gmail.com");
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	
	public recordandsendHome sendBtn() throws InterruptedException {
		Thread.sleep(2000);
		sendBtn.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}

	public recordandsendHome saveAndExit() throws InterruptedException {
		Thread.sleep(2000);
		saveExitBtn.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	
	public recordandsendHome companyDetails() throws InterruptedException {
		Thread.sleep(2000);
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	
	


	
}