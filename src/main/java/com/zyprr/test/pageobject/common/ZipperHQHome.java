package com.zyprr.test.pageobject.common;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.zyprr.test.base.factory.ZyprrPageFactory;
import com.zyprr.test.base.pageobject.BasePageObject;
import com.zyprr.test.config.Config;
import com.zyprr.test.pageobject.calendar.CalendarHome;
import com.zyprr.test.pageobject.contact.AddContacts;
import com.zyprr.test.pageobject.contact.ContactsHome;
import com.zyprr.test.pageobject.util.WaitUtil;
import com.zyprr.test.utility.ZyprrUtilities;


public class ZipperHQHome extends BasePageObject {
	
//	private static final String LOGIN_URL="https://tautomation.zipperhq.com";
//	private static final String LOGIN_URL_ESTATE="/mls/search?_zr=2017-06-23-05-37";

//	http://naisha.newmobile.nimbusworx.net/
	// http://naisha.newmobile.nimbusworx.net/reopp/rental?l=rent&sidx=0&ps=10&_zr=2017-06-05-08-55

	public ZipperHQHome(WebDriver webDriver) {
		super(webDriver);
			
	}
	
      
	@CacheLookup
	@FindBy(id = "dashboard")
	WebElement dashboard;
	
	@CacheLookup
	@FindBy(id = "contacts")
	WebElement Contacts;

    @CacheLookup
	@FindBy(id = "videos")
	WebElement Videos;
	
	@CacheLookup
	@FindBy(id = "templates")
	WebElement Templates;
	
	@CacheLookup
	@FindBy(id = "campaigns")
	WebElement Campaigns;
	
	@CacheLookup
	@FindBy(id = "trends")
	WebElement Trends;
	
	@CacheLookup
	@FindBy(id = "recordButtonNav")
	WebElement RecordAndSend;
	
	@CacheLookup
	@FindBy(id = "userProfileInitials")
	WebElement userIcon;
	
	@CacheLookup
	@FindBy(id = "lH_contact_bttns_new")
	WebElement quickAddButton;
	
	@CacheLookup
	@FindBy(id = "HQ_profile-Logout")
	WebElement logOutBtn;
	
	@ CacheLookup
	@ FindBy(id = "username")
	WebElement	userName ;

	@ CacheLookup
	@ FindBy(id = "password-field")
	WebElement	password ;

	@ CacheLookup
	@ FindBy(id = "rememberMe1")
	WebElement	rememberMe ;

	@ CacheLookup
	@ FindBy(id = "zhqSignIn")
	WebElement	loginButton ;
	
	@ CacheLookup
	@ FindBy(id = "social_google_login")
	WebElement	socialGoogleLogin ;
	
	@ CacheLookup
	@ FindBy(id = "firstSplashClsBtn")
	WebElement	productTourNext ;
	
	@ CacheLookup
	@ FindBy(xpath = "/html/body/div[8]/div/div[5]")
	WebElement	productTourClose ;
	

	@ CacheLookup
	@ FindBy(id = "industryDropdown")
	WebElement	industryDropdown ;
	
	
	@ CacheLookup
	@ FindBy(xpath = "//*[@id=\"industryDropdown\"]/option[6]")
	WebElement	industryDropdownTechnology ;
	
	

	@ CacheLookup
	@ FindBy(id = "departmentDropdown")
	WebElement	departmentDropdown ;  
	
	

	@ CacheLookup
	@ FindBy(xpath = "//*[@id=\"departmentDropdown\"]/option[2]")
	WebElement	departmentDropdownManagement ;
	
	
	
	@ CacheLookup
	@ FindBy(id = "roleDropdown")
	WebElement	roleDropdown ; 
  
	
	
	@ CacheLookup
	@ FindBy(xpath = "//*[@id=\"roleDropdown\"]/option[14]")
	WebElement	roleDropdownStudent ;
	
	
	

	@ CacheLookup
	@ FindBy(id = "companyName")
	WebElement	companyName ; 
	
	@ CacheLookup
	@ FindBy(xpath = "//*[@id=\"companyDetailsFormModal\"]/div/div/div[2]/div[2]/button")
	WebElement requiredInputFieldsOk ;
  
	
	@ CacheLookup
	@ FindBy(xpath = "//*[@id=\"splashDemo\"]/div/div[1]/div[2]/button")
	WebElement  chromeExtensionNext ;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"mediaDiv\"]/div[1]/div[1]/div/div[2]/div/div[2]/div[2]/div/ul/li[1]/div[3]/div[3]/a/i")
	WebElement videoEditButton;
	

	
	
//	private FluentWait<WebDriver> wait;
	
	public AddContacts xferToContactHomePage() throws InterruptedException {
		Contacts.click();
		Thread.sleep(2000);
		quickAddButton.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				AddContacts.class);
	}

	
	
	private ZipperHQHome clickQuickAddButton() throws InterruptedException {
		Thread.sleep(4000);
		quickAddButton.click();
		
		return this;
	}
	
	public ContactsHome xferToContactHomePage1() throws InterruptedException {
		Thread.sleep(2000);
		Contacts.click();
		
		
		return null;
		
	}
	
	public videoHome xferToVideoHomePage() throws InterruptedException {
		Thread.sleep(2000);
		Videos.click();
		
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				videoHome.class);

	}
	
	public  DashboardHome xferToDashboardHomePage() throws InterruptedException {
		Thread.sleep(2000);
		dashboard.click();
		
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				DashboardHome.class);

	}
	

	public  trendsHome xferToTrendsHomePage() throws InterruptedException {
		Thread.sleep(2000);
		Trends.click();
		
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				trendsHome.class);

	}
	
	public  recordandsendHome xferTorecordandsendHomePage() throws InterruptedException {
		Thread.sleep(2000);
		RecordAndSend.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);

	}

	
	
	

	


	@Override
	protected void init() {
	}

	@Override
	public void waitForPageLoad() {
//		WaitUtil.getInstance().waitForElementTobeClickable(webDriver,
//		expandBtn);
		}

    @Override
	protected WebElement[] getPageLoadIndicatorElements() {
		// TODO Auto-generated method stub
		return null;
	}
    
    private void clickUserIcon() {
    	userIcon.click();
	}
    
    public ZipperHQHome logOut() throws InterruptedException {
    	
    	Thread.sleep(4000);
    	clickUserIcon();
    	Thread.sleep(3000);
    	

	
		logOutBtn.click();
		return null;
	}

	public Object login1() throws InterruptedException {
		
		
		  
		    Thread.sleep(2000);
		    userName.sendKeys("zyprr.local+020@gmail.com");
		    Thread.sleep(2000);
		    password.sendKeys("zipperhq123");
	        Thread.sleep(2000);
	        loginButton.click();
	        Thread.sleep(2000);
	       industryDropdown.click();
	       Thread.sleep(2000);
	       industryDropdownTechnology.click();
	       departmentDropdown.click();
	       departmentDropdownManagement.click();
	       roleDropdown.click();
	        roleDropdownStudent.click();
	        companyName.sendKeys("Zyprr");
	        requiredInputFieldsOk.click();
	       Thread.sleep(2000);
	        chromeExtensionNext.click();
	        Thread.sleep(2000);
	        productTourNext.click();
	       Thread.sleep(2000);
	        productTourClose.click();
	        Thread.sleep(2000);
	        
	        
	        
	        
	       
		 
		// TODO Auto-generated method stub
		return null;
	}
	
}


