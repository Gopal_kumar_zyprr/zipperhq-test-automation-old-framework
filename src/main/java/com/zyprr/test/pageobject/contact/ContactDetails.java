package com.zyprr.test.pageobject.contact;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import com.zyprr.test.base.factory.ZyprrPageFactory;
import com.zyprr.test.base.pageobject.BasePageObject;
import com.zyprr.test.pageobject.common.WaitUtil;
import com.zyprr.test.utility.ZyprrUtilities;

public class ContactDetails extends BasePageObject {

	public ContactDetails(WebDriver webDriver) {
		super(webDriver);
	}

	public final String xpShareWith = "(//div[@ng-repeat='obj in serviceData.shareList'])";

	private static final String xpSubject = "(//a[@class='opp-name ng-binding'])";
	private static final String xpTilte = "(//span[@class='zy-notes ng-binding'])";
	private static final String xpClickStatusBtn = "(//i[@class='fa fa-angle-right'])";
	private static final String xpCheckStatus = "(//span[@class='zy-span-value zy-break-word zy-status ng-binding'])";
	private static final String xpDeleteBtn = "(//i[@class='fa fa-trash ng-scope'])";

	@CacheLookup
	@FindBy(id = "con_dv_fn")
	WebElement contactName;

	@CacheLookup
	@FindBy(id = "con_a_accnm")
	WebElement accountName;

	@CacheLookup
	@FindBy(id = "con_dv_title")
	WebElement title;

	@CacheLookup
	@FindBy(id = "con_dv_lds")
	WebElement leadSource;

	@CacheLookup
	@FindBy(id = "con_dv_inds")
	WebElement industry;

	@CacheLookup
	@FindBy(xpath = "(//div[@id='con_dv_cs'])[1]")
	WebElement status;

	// @CacheLookup
	// @FindBy(id = "con_dv_cs")

	// WebElement status;

	@CacheLookup
	@FindBy(id = "con_dv_poff")
	WebElement primaryPhone;

	@CacheLookup
	@FindBy(id = "con_dv_pf")
	WebElement fax;

	@CacheLookup
	@FindBy(id = "con_dv_pm")
	WebElement phoneMobile;

	@CacheLookup
	@FindBy(id = "con_a_ew1")
	WebElement primaryEmail;

	@CacheLookup
	@FindBy(id = "con_dv_ew2")
	WebElement otherEmail;

	@CacheLookup
	@FindBy(id = "con_dv_po")
	WebElement otherPhone;

	@CacheLookup
	@FindBy(id = "con_dv_wb")
	WebElement website;

	@CacheLookup
	@FindBy(id = "con_dv_pmadd1")
	WebElement primaryAddressLine1;

	@CacheLookup
	@FindBy(id = "con_dv_pmadd2")
	WebElement primaryAddressLine2;

	// @CacheLookup
	// @FindBy(@id="con_dv_cs")
	// WebElement primaryAddress;

	@CacheLookup
	@FindBy(xpath = "//label[text()='Primary Address']/../div[@class='ng-binding ng-scope']")
	WebElement primaryAddress;

	@CacheLookup
	@FindBy(id = "con_dv_country")
	WebElement primaryCountry;

	@CacheLookup
	@FindBy(id = "con_dv_altadd1")
	WebElement alternateAddressLine1;

	@CacheLookup
	@FindBy(id = "con_dv_altadd2")
	WebElement alternateAddressLine2;

	@CacheLookup
	@FindBy(id = "con_dv_altcs")
	WebElement alternateAddress;

	@CacheLookup
	@FindBy(id = "con_dv_altcountry")
	WebElement alternateCountry;

	@CacheLookup
	@FindBy(id = "con_dv_des")
	WebElement description;

	@CacheLookup
	@FindBy(id = "con_dv_oi")
	WebElement ownerName;

	@CacheLookup
	@FindBy(id = "con_dv_oi")
	WebElement assignedTo;

	@CacheLookup
	@FindBy(id = "con_dv_bd")
	WebElement birthDate;

	@CacheLookup
	@FindBy(id = "con_dv_rb")
	WebElement referredBy;

	@CacheLookup
	@FindBy(id = "con_dv_ass")
	WebElement assistant;

	@CacheLookup
	@FindBy(id = "con_dv_ap")
	WebElement assistantPhone;

	@CacheLookup
	@FindBy(xpath = "//label[text()='text']/../div[@class='col-lg-9 col-sm-7 col-md-7 lnop']")
	WebElement text;

	@CacheLookup
	@FindBy(xpath = "//label[text()='num']/../div[@class='col-lg-9 col-sm-7 col-md-7 lnop']")
	WebElement num;

	@CacheLookup
	@FindBy(xpath = "//label[text()='First Information']/../div[@class='col-lg-9 col-sm-7 col-md-7 lnop']")
	WebElement firstInformation;

	@CacheLookup
	@FindBy(xpath = "//label[text()='Money']/../div[@class='col-lg-9 col-sm-7 col-md-7 lnop']")
	WebElement money;

	@CacheLookup
	@FindBy(xpath = "//label[text()='Restricted']/../div[@class='pull-left ng-scope']")
	WebElement restricted;

	@CacheLookup
	@FindBy(id = "con_dv_zys")
	WebElement shareWith;

	@CacheLookup
	@FindBy(xpath = "//label[text()='Active']/../div[@class='pull-left ng-scope']")
	WebElement active;

	// @CacheLookup
	// @FindBy(id = "quickActionNote")
	// WebElement notesTab;

	@CacheLookup
	@FindBy(id = "contactNoteCount")
	WebElement notesCount;

	@CacheLookup
	@FindBy(id = "contactTaskCount")
	WebElement taskCount;

	@CacheLookup
	@FindBy(id = "quickActionCall")
	WebElement callsTab;

	@CacheLookup
	@FindBy(id = "contactCallLogCount")
	WebElement callsCount;

	@CacheLookup
	@FindBy(id = "quickActionMeeting")
	WebElement meetingsTab;

	@CacheLookup
	@FindBy(id = "contactMeetingCount")
	WebElement meetingCount;

	/*@CacheLookup
	@FindBy(id = "quickActionSupport")
	WebElement supportTab;*/
	

	@CacheLookup
	@FindBy(xpath = "(//a[@ng-click='sd.related = namedObject; ns.buildRelation(dataId, srvc.type);'])[4]")
	WebElement supportTab;

	@CacheLookup
	@FindBy(id = "contactCaseCount")
	WebElement supportCount;

	@CacheLookup
	@FindBy(id = "rltd_anc_oppcontact")
	WebElement opportunityTab;
	
	@CacheLookup
	@FindBy(id = "rltd_anc_reopp")
	WebElement opportunityTab1;

	@CacheLookup
	@FindBy(xpath = "//div[@id='data_0']/a[contains(@href,'/opportunity/view/')]")
	WebElement opportunityName;

	@CacheLookup
	@FindBy(id = "contactOppCount")
	WebElement opportunityCount;

	@CacheLookup
	@FindBy(id = "rltd_anc_emailLink")
	WebElement emailTab;
	
	@CacheLookup
	@FindBy(id = "rltd_anc_attachment")
	WebElement documentTab;
	
	@CacheLookup
	@FindBy(id = "cmp-eml-btn")
	WebElement emailCompose;

	@CacheLookup
	@FindBy(id = "contactEmailCount")
	WebElement emailCount;

	@CacheLookup
	@FindBy(id = "social")
	WebElement socialNtwHeader;

	@CacheLookup
	@FindBy(id = "con_a_fb")
	WebElement facebookID;

	@CacheLookup
	@FindBy(id = "con_a_lnk")
	WebElement linkedinID;

	@CacheLookup
	@FindBy(id = "con_a_gdplus")
	WebElement googlePlusID;

	@CacheLookup
	@FindBy(id = "con_a_twt")
	WebElement twitterID;

	@CacheLookup
	@FindBy(id = "con_a_skype")
	WebElement skypeID;

	@CacheLookup
	@FindBy(id = "con_btn_edt")
	WebElement editBtn;

	@CacheLookup
	@FindBy(xpath = "(//a[@class='opp-name ng-binding'])[1]")
	WebElement subject;

	@CacheLookup
	@FindBy(xpath = "//select[@ng-model='cobj.activityStatus']")
	WebElement activityStatus;

	/*@CacheLookup
	@FindBy(xpath = "//div[@class='mxwidth']")
	WebElement subjectName;*/
	
	@CacheLookup
	@FindBy(xpath = "//a[@id='cscr_a_0']")
	WebElement subjectName;

	// For Comment Button

	@CacheLookup
	@FindBy(xpath = "//div[@id='singlethread']")
	WebElement commentBtn;

	@CacheLookup
	@FindBy(id = "addComment")
	WebElement addComment;

	@CacheLookup
	@FindBy(id = "saveComment")
	WebElement saveComment;

	@CacheLookup
	@FindBy(xpath = "//div[@class='Ctext col-lg-12 col-sm-12 col-md-12 col-xs-12 nopadding']")
	WebElement viewComment;

	@CacheLookup
	@FindBy(xpath = "//span[@class='own']")
	WebElement deleteComment;

	@CacheLookup
	@FindBy(id = "con_span_rt")
	WebElement relatedTo;

	@CacheLookup
	@FindBy(xpath = "//a[@aria-controls='activity']")
	WebElement activitiesTab;

	@CacheLookup
	@FindBy(id = "rltd_anc_note")
	WebElement notesTab;
	
	@CacheLookup
	@FindBy(id = "addTagInput1")
	WebElement tagInputFld1;
	
	@CacheLookup
	@FindBy(id = "dia_btn_sv")
	WebElement tagAddBtn1;

//	@CacheLookup
//	@FindBy(id = "tg_i")
//	WebElement tagAddBtn;

	@CacheLookup
	@FindBy(id = "avy_a_2")
	WebElement callTab;

	@CacheLookup
	@FindBy(xpath = "//button[@class='zq-btn logogreen pull-right']")
	WebElement statusYesBtn;

	@CacheLookup
	@FindBy(id = "avy_a_3")
	WebElement meetingTab;
	
	@CacheLookup
	@FindBy(id = "avy_a_4")
	WebElement showingTab;

	@CacheLookup
	@FindBy(id = "avy_a_1")
	WebElement taskTab;

	@CacheLookup
	@FindBy(id = "rltd_aa")
	WebElement tagTab;
	
	@CacheLookup
	@FindBy(id = "rltd_anc_case")
	WebElement caseTab;
	

	// @CacheLookup

	// @FindBy(id = "activityRltdId")
	// WebElement newBtn;
	
	@CacheLookup
	@FindBy(id = "opc_ip")
	WebElement relateOpp;

	@CacheLookup
	@FindBy(id = "avy_bt")
	WebElement newBtn;

	@CacheLookup
	@FindBy(id = "not_bt")
	WebElement newNoteBtn;

	@CacheLookup
	@FindBy(id = "dia_btn_del")
	WebElement deleteBtn;

	@CacheLookup
	@FindBy(id = "dia_btn_unrel")
	WebElement unrelateBtn;

	@CacheLookup
	@FindBy(xpath = "//i[@ng-click='completer.lookup(rsd.data.actName);']")
	WebElement relateToSearchBtn;

	@CacheLookup
	@FindBy(id = "searchStr")
	WebElement inputLookupStr;

	@CacheLookup
	@FindBy(xpath = "(//i[@class='fa fa-search'])[2]")
	WebElement tblSearchBtn;

	@CacheLookup
	@FindBy(id = "lookup_search_value_tableBody")
	WebElement lkpTableHeader;

	@CacheLookup
	@FindBy(xpath = "(//td[@ng-repeat='element in model.meta.lookupable.elements'])[2]")
	WebElement tblLookupStrSubject;

	@CacheLookup
	@FindBy(xpath = "(//td[@class='zt-sml'])[1]")
	WebElement tblLookupStrType;

	@CacheLookup
	@FindBy(xpath = "//button[@class='btn btn-danger ng-scope']")
	WebElement closeLookupBtn;

	@CacheLookup
	@FindBy(xpath = "//select[@ng-model='cobj.activityStatus']")
	WebElement statusTask;

	@CacheLookup
	@FindBy(xpath = "(//button[@class='btn btn-primary'])[4]")
	WebElement noteDeleteBtn;

	@CacheLookup
	@FindBy(id = "tg_i")
	WebElement tagAddBtn;

	@CacheLookup
	@FindBy(id="tg_ip1")
	WebElement tagInputFld;

	@CacheLookup
	@FindBy(xpath = "(//div[@class='zy-tag ng-binding ng-scope'])[1]")
	WebElement tags;

	@CacheLookup
	@FindBy(id = "con_span_view")
	WebElement closeBtn;
	
	@CacheLookup
	@FindBy(id = "ath_bt")
	WebElement docAddBtn;
	
	@CacheLookup
	@FindBy(id = "cs_i")
	WebElement caseAddBtn;
	
	@CacheLookup
	@FindBy(id = "reopp_ad")
	WebElement oppAddBtn;
	
	private ContactDetails clickRelateBtn() {
		this.relateToSearchBtn.click();
		return this;
	}

	private ContactDetails getRelatedTo() {
		this.relatedTo.click();
		return this;
	}

	private ContactDetails getActivitiesTab() {
		this.activitiesTab.click();
		return this;
	}

	private ContactDetails setAddComment(String comment) {
		this.addComment.sendKeys(comment);
		return this;
	}

	private ContactDetails pressSaveCommentButton() {
		this.saveComment.click();
		return this;

	}
	
	// private ContactDetails verifyComment() {
	// this.verifyComment.click();
	// return this;
	//
	// }

	private ContactDetails deleteComment() {
		this.deleteComment.click();
		return this;

	}

	private ContactDetails clickStatusBtn(int i) {

		String xPathStatus = xpClickStatusBtn + "[" + i + "]";

		webDriver.findElement(By.xpath(xPathStatus)).click();
		return this;
	}

	// for adding comment

	public ContactDetails addComment(Map<String, Object> inputData,
			String[] fieldNames) throws InterruptedException {
		this.commentBtn.click();
		// Thread.sleep(4000);
		setAddComment((String) inputData.get(fieldNames[3]));
		pressSaveCommentButton();

		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}

	// for verifying comment

	// public ContactDetails getComment() throws InterruptedException {
	// this.commentBtn.click();
	// Thread.sleep(4000);
	// verifyComment();
	//
	// return ZyprrPageFactory.getInstance().getPageObject(webDriver,
	// ContactDetails.class);
	// }

	public WebElement getViewComment() throws InterruptedException {
		this.commentBtn.click();
		Thread.sleep(4000);
		return viewComment;
	}

	public ContactDetails deleteVerifiedComment() throws InterruptedException {
		Thread.sleep(4000);
		deleteComment();

		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}

	public String getShareWithDataFromScreen(int i) {

		String xpath = xpShareWith + "[" + i + "]";

		String valueFromScreen = webDriver.findElement(By.xpath(xpath))
				.getText();

		return valueFromScreen;
	}

	// For verification of Comment
	public Map<Integer, WebElement> getCommentElements() {

		Map<Integer, WebElement> CommentElements = new HashMap<Integer, WebElement>();

		CommentElements.put(3, addComment);

		return CommentElements;
	}
	
	public ContactDetails addTag1(String tagString) throws InterruptedException {
		tagInputFld1.sendKeys(tagString);
		tagAddBtn1.click();
		Thread.sleep(4000);
		ZyprrUtilities.takeDriverSnapShot(webDriver, webDriver.getTitle());
		return this;
	}

	public ContactDetails getNotesTab() throws InterruptedException {

		getRelatedTo().notesTab.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}

	public ContactDetails getTaskTab() throws InterruptedException {
		getRelatedTo().getActivitiesTab().taskTab.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}

	public ContactDetails getCallTab() throws InterruptedException {
		getRelatedTo().getActivitiesTab().callTab.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}

	public ContactDetails getMeetingTab() throws InterruptedException {
		getRelatedTo().getActivitiesTab().meetingTab.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}
	
	public ContactDetails getShowingTab() throws InterruptedException {

		getRelatedTo().activitiesTab.click();
		this.showingTab.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}

	public ContactDetails getTagTab() throws InterruptedException {
		getRelatedTo().tagTab.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}

	
	public ContactDetails getOpportunityTab() {
		getRelatedTo().opportunityTab.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}
	
	public ContactDetails getOpportunityTab1() {
		getRelatedTo().opportunityTab1.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}
	
	public ContactDetails getDocumentTab() {
		getRelatedTo().documentTab.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}
	
	public ContactDetails getCaseTab() {
		getRelatedTo().caseTab.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}

	
	

	public int getActivitiesCount() {

		int rowCount = webDriver.findElements(
				By.xpath("(//a[@class='opp-name ng-binding'])")).size();

		return rowCount;
	}

	public ContactDetails changeStatus(int i) {
		clickStatusBtn(i);
		this.activityStatus.sendKeys("Completed");
		this.statusYesBtn.click();
		return this;
	}

	public String getActivitiesSubject(int i) {

		String xPathSubject = xpSubject + "[" + i + "]";
		String subject = webDriver.findElement(By.xpath(xPathSubject))
				.getText();
		return subject;
	}
	
	
	public String checkStatus(int i) {

		String xPathStatus = xpCheckStatus + "[" + i + "]";

		String statusFromScreen = webDriver.findElement(By.xpath(xPathStatus))
				.getText();
		return statusFromScreen;
	}

	public ContactDetails clickDeleteBtn(int i, String activity) {

		String xPathDelete = xpDeleteBtn + "[" + i + "]";

		webDriver.findElement(By.xpath(xPathDelete)).click();
		if (activity.equalsIgnoreCase("Delete")) {
			deleteBtn.click();
		}
		if (activity.equalsIgnoreCase("Unrelate")) {
			unrelateBtn.click();
		}

		return this;
	}

	public ContactDetails verifyLookupSubject(String task)
			throws InterruptedException {

		clickRelateBtn().setLookupStr(task).tblSearchBtn.click();
		WaitUtil.getInstance().waitForElementTobeClickable(webDriver,
				lkpTableHeader);
		return this;
	}

	private ContactDetails setLookupStr(String task) {
		this.inputLookupStr.sendKeys(task);
		return this;
	}

	public String verifyLookupGetType() throws InterruptedException {

		String type = tblLookupStrType.getText();
		Thread.sleep(2000);
		this.closeLookupBtn.click();
		return type;
	}

	public ContactDetails unrelateSubject() throws InterruptedException {

		this.unrelateBtn.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}

	public WebElement getSubjectNameInList() {

		return tblLookupStrSubject;
	}

	public int getNumberNotesCount() {

		int rowCount = webDriver.findElements(
				By.xpath("(//span[@class='zy-notes ng-binding'])")).size();

		return rowCount;
	}

	public String getNotes(int i) {

		String xPathSubject = xpTilte + "[" + i + "]";
		String subject = webDriver.findElement(By.xpath(xPathSubject))
				.getText();
		return subject;
	}

	public ContactDetails clickNotesDeleteBtn(int i) {

		String xPathDelete = xpDeleteBtn + "[" + i + "]";

		webDriver.findElement(By.xpath(xPathDelete)).click();

		noteDeleteBtn.click();

		return this;
	}

	public ContactDetails addTag(String tagString) {
		tagInputFld.sendKeys(tagString);
		tagAddBtn.click();
		ZyprrUtilities.takeDriverSnapShot(webDriver, webDriver.getTitle());
		return this;
	}

	public WebElement getTagFromScreen() {
		return tags;
	}

	public ContactDetails closeBtn() throws InterruptedException {

		this.closeBtn.click();

		return this;
	}

	public Map<Integer, WebElement> getContactElements() {
		Map<Integer, WebElement> contactElements = new HashMap<Integer, WebElement>();
		contactElements.put(1, contactName);
		contactElements.put(2, contactName);
		contactElements.put(3, contactName);
		contactElements.put(4, title);
		contactElements.put(5, accountName);
		contactElements.put(6, leadSource);
		contactElements.put(7, status);
		// contactElements.put(6, department) ;
		contactElements.put(8, industry);
		contactElements.put(9, phoneMobile);
		contactElements.put(10, primaryPhone);
		contactElements.put(11, otherPhone);
		contactElements.put(12, fax);
		contactElements.put(13, primaryEmail);
		contactElements.put(14, otherEmail);
		contactElements.put(15, website);
		// contactElements.put(16, facebookID) ;
		// contactElements.put(17, linkedinID) ;
		// contactElements.put(18, twitterID) ;
		// contactElements.put(19, googlePlusID) ;
		// contactElements.put(20, skypeID) ;
		contactElements.put(21, primaryAddressLine1);
		contactElements.put(22, primaryAddressLine2);
		contactElements.put(23, primaryAddress);
		contactElements.put(24, primaryAddress);
		contactElements.put(25, primaryAddress);
		contactElements.put(26, primaryCountry);
		contactElements.put(27, alternateAddressLine1);
		contactElements.put(28, alternateAddressLine2);
		contactElements.put(29, alternateAddress);
		contactElements.put(30, alternateAddress);
		contactElements.put(31, alternateAddress);
		contactElements.put(32, alternateCountry);
		contactElements.put(33, description);
		contactElements.put(34, ownerName);
		contactElements.put(35, assignedTo);
		// contactElements.put(36, birthDate);
		contactElements.put(37, referredBy);
		contactElements.put(38, assistant);
		contactElements.put(39, assistantPhone);
		// contactElements.put(40, text);
		// contactElements.put(41, num);
		// contactElements.put(42, firstInformation);
		// contactElements.put(43, money);
		contactElements.put(44, restricted);
		contactElements.put(45, shareWith);
		contactElements.put(46, active);

		return contactElements;

	}

	public Map<Integer, WebElement> getQuickAddContactElements() {
		Map<Integer, WebElement> contactElements = new HashMap<Integer, WebElement>();
		contactElements.put(1, contactName);

		contactElements.put(3, contactName);
		contactElements.put(4, accountName);

		contactElements.put(10, primaryPhone);

		contactElements.put(12, primaryEmail);

		contactElements.put(17, primaryAddressLine1);
		contactElements.put(19, primaryAddress);

		contactElements.put(20, primaryAddress);
		contactElements.put(21, primaryAddress);

		contactElements.put(22, primaryCountry);

		return contactElements;
	}

	public WebElement getDescription() {
		webDriver.switchTo().frame(0);
		return this.description;
	}

	public ContactDetails clickEmailTab() throws InterruptedException {
		getRelatedTo().emailTab.click();
		// Thread.sleep(4000) ;
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);

	}
	
	

	public ContactDetails getSupportTab() {
		this.relatedTo.click();
		this.supportTab.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				ContactDetails.class);
	}

	// public ContactDetails getCallsTab() {
	// this.callsTab.click();
	// return ZyprrPageFactory.getInstance().getPageObject(webDriver,
	// ContactDetails.class);
	// }

	

	public WebElement getNotesCount() {
		return notesCount;
	}

	public WebElement getcallsCount() {
		return callsCount;
	}

	public WebElement getEmailCount() {
		return emailCount;
	}

	public WebElement getmeetingsCount() {
		return meetingCount;
	}

	public WebElement getTasksCount() {
		return taskCount;
	}

	// public AddCalls clickCallTab() throws InterruptedException {
	// this.callsTab.click();
	// return ZyprrPageFactory.getInstance().getPageObject(webDriver,
	// AddCalls.class);
	// }
	//
	// public AddMeetings clickMeetingTab() throws InterruptedException {
	// this.meetingsTab.click();
	// return ZyprrPageFactory.getInstance().getPageObject(webDriver,
	// AddMeetings.class);
	//
	// }

	public ContactDetails getSocialNtwHeader() throws InterruptedException {
		socialNtwHeader.click();
		WaitUtil.getInstance().waitForElementTobeVisible(webDriver, skypeID);
		return this;
	}

	public WebElement getSubject() {
		return subject;
	}

	public WebElement getSubjectName() {
		return subjectName;
	}

	public WebElement getOpportunityName() {
		return opportunityName;
	}


	
	
	

	@Override
	protected WebElement[] getPageLoadIndicatorElements() {
		WebElement[] loadIndicatorElements = new WebElement[1];
		loadIndicatorElements[0] = editBtn;
		return loadIndicatorElements;
	}

	@Override
	protected void init() {
	}

	@Override
	public void waitForPageLoad() {
		WebElement[] loadIndicatorElements = getPageLoadIndicatorElements();
		WaitUtil.getInstance().waitForElementTobeVisible(webDriver,
				loadIndicatorElements[0]);

	}

}
