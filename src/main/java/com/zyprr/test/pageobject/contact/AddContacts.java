package com.zyprr.test.pageobject.contact;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import com.zyprr.test.base.factory.ZyprrPageFactory;
import com.zyprr.test.base.pageobject.BasePageObject;
import com.zyprr.test.pageobject.common.WaitUtil;
import com.zyprr.test.utility.ZyprrUtilities;

public class AddContacts extends BasePageObject {

	public AddContacts(WebDriver webDriver) {
		super(webDriver);

	}
	
	@CacheLookup
	@FindBy(id = "contact_add_modal_first_name")
	WebElement FirstName;
	

	@CacheLookup
	@FindBy(id = "contact_add_modal_last_name")
	WebElement LastName;
	
	@CacheLookup
	@FindBy(id = "contact_add_modal_email_work1")
	WebElement Email;
	
	@CacheLookup
	@FindBy(id = "contact_add_modal_acnt_name")
	WebElement Company;
	
	@CacheLookup
	@FindBy(id = "contact_add_modal_phone")
	WebElement Phone;
	
	@CacheLookup
	@FindBy(id = "contact_add_modal_savenew_btn")
	WebElement SaveAndAddNew;
	
	@ CacheLookup
	@ FindBy(xpath = "contact_add_modal_cls_btn")
	WebElement	contactAddModleClose;
	
	
	
	

	


	
	private AddContacts setFirstName(String FirstName) {
		this.FirstName.clear();
		this.FirstName.sendKeys(FirstName);
		return this;
	}

	private AddContacts setLastName(String LastName) {
		this.LastName.clear();
		this.LastName.sendKeys(LastName);
		return this;
	}
	
	private AddContacts setEmail(String Email) {
		this.Email.clear();
		this.Email.sendKeys(Email);
		return this;
	}

	
    private AddContacts setPhone(String Phone) {
		this.Phone.clear();
		this.Phone.sendKeys(Phone);
		return this;
	}
	
   private AddContacts setCompany(String Company) {
		this.Company.clear();
		this.Company.sendKeys(Company);
		return this;
	}
	public AddContacts createNewContact(Map<String, Object> inputData,
			String[] fieldNames) throws InterruptedException {
	             setFirstName((String) inputData.get(fieldNames[0]))
				.setLastName((String) inputData.get(fieldNames[1]))
				.setEmail((String) inputData.get(fieldNames[2]))
				.setPhone((String) inputData.get(fieldNames[3]))
				.setCompany((String) inputData.get(fieldNames[4]));
        ZyprrUtilities.takeDriverSnapShot(webDriver, webDriver.getTitle());
		pressSaveAndAddNew();
		Thread.sleep(2000);
		
		

		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
			
				AddContacts.class);
	}
	
	private AddContacts pressSaveAndAddNew() throws InterruptedException {
		this.SaveAndAddNew.click();
		Thread.sleep(4000);
		// pressCreateAccountPopUp();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				AddContacts.class);
	}
	
	
	
	

	@Override
	protected WebElement[] getPageLoadIndicatorElements() {
		WebElement[] loadIndicatorElements = new WebElement[1];
		loadIndicatorElements[0] = SaveAndAddNew;

		
		return loadIndicatorElements;
	}

	@Override
	protected void init() {
	}

	@Override
	public void waitForPageLoad() {
		WebElement[] loadIndicatorElements = getPageLoadIndicatorElements();
		WaitUtil.getInstance().waitForElementTobeVisible(webDriver,
				loadIndicatorElements[0]);
		
	}
}
