package com.zyprr.test;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.zyprr.test.pageobject.common.ZipperHQHome;

import com.zyprr.test.testcase.abs.AbstractTest;

public class ZipperHQLoggout extends AbstractTest {

	@Test
	public void logoutTest() throws InterruptedException {
		ZipperHQHome zyprrHomePage = PageFactory.initElements(webDriver,
				ZipperHQHome.class);
		//ZyprrUtilities.takeDriverSnapShot(webDriver, webDriver.getTitle());

		zyprrLogger = zyprrReport.startTest("LogOutTest");
		zyprrLogger.log(LogStatus.INFO, "Clicked on Logout");
		zyprrHomePage.logOut();
		Thread.sleep(2000);
		//ZyprrUtilities.takeDriverSnapShot(webDriver, webDriver.getTitle());

	}
	
	//Added for Report
	@AfterClass
	public void tearDown() {
		zyprrReport.endTest(zyprrLogger);
		zyprrReport.flush();

	}
	// End of addition for Report
	
	@Override
	protected String getDataSheetName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String[] getFieldNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getDataPath() {
		// TODO Auto-generated method stub
		return null;
	}

}
