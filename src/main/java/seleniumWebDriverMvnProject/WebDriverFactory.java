package seleniumWebDriverMvnProject;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.zyprr.test.config.Config;

public class WebDriverFactory {
	private static final String INIT_URL = "http://stephen.zenzuzu.com/";
	private static WebDriverFactory instance = new WebDriverFactory();
    private WebDriver webDriver = null;
	private FirefoxDriver fireFoxDriver = null;
	private InternetExplorerDriver ieDriver = null;
	private ChromeDriver chromeDriver = null;

	private WebDriverFactory() {

	}

	public static WebDriverFactory getInstance() {
		return instance;
	}
	
//	synchronized public WebDriver getHtmlUnitDriver() {
//		if (webDriver == null){
//		//	HtmlUnitDriver driver = new HtmlUnitDriver();
//
//			HtmlUnitDriver driver = new HtmlUnitDriver();
//			driver.setJavascriptEnabled(true);
//			webDriver = driver;
////					webDriver.setJavascriptEnabled(true);
//		}
//		System.out.println("returning web driver");
//		return webDriver;
//	}
		
	
//	synchronized public WebDriver getPhantomJSDriver() {
//		if (webDriver == null){
//             Capabilities caps = new DesiredCapabilities();
//             ((DesiredCapabilities) caps).setJavascriptEnabled(true);                
//             ((DesiredCapabilities) caps).setCapability("takesScreenshot", true);  
//             /*((DesiredCapabilities) caps).setCapability(
//                     PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
//                     "C:/Program Files/phantomjs-2.1.1-windows/bin/phantomjs.exe"
//                 );*/
//             ((DesiredCapabilities) caps).setCapability(
//                     PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
//                     Config.getInstance().getPhantomjsDriverPath()
//                 );
//             
//             
//////             caps.setCapability(CapabilityType.PROXY, pro);
//        //     ((DesiredCapabilities) caps).setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//       //      ((DesiredCapabilities) caps).setCapability(CapabilityType.TAKES_SCREENSHOT, true);
//        //    ((DesiredCapabilities) caps).setCapability(CapabilityType.SUPPORTS_FINDING_BY_CSS, true);
//           // ((DesiredCapabilities) caps).setCapability(PhantomJSDriverService.PHANTOMJS_CLI//_ARGS, new String[] { "--ignore-ssl-errors=yes" });
//            // ((DesiredCapabilities) caps).setCapability("browserType", "phantomjs");
//             
//			PhantomJSDriver driver = new PhantomJSDriver(caps);
//		//	driver.manage().window().setSize(new Dimension(1400,1000));
//			webDriver = driver;
//
//		}
//		
//		return webDriver;
//	}

	synchronized public FirefoxDriver getFireFoxDriver() {
		if (fireFoxDriver == null) {

			fireFoxDriver = new FirefoxDriver();
			// End of comment by Debasish
			fireFoxDriver.manage().timeouts()
					.implicitlyWait(10, TimeUnit.SECONDS);

		}
		System.out.println("returning firefox driver");
		return fireFoxDriver;
	}
	
	
//	public FirefoxDriver getFireFoxDriver (){
//		if (fireFoxDriver != null) {
//			return fireFoxDriver;
//		}
//		
//		FirefoxBinary binary = new FirefoxBinary(new File("C:/Program Files (x86)/Mozilla Firefox/firefox.exe"));
//		binary.setEnvironmentProperty("DISPLAY", ":0");
//		 fireFoxDriver = new FirefoxDriver(binary,null);
//		 
//		 return fireFoxDriver;
//		}

	synchronized public InternetExplorerDriver getIEDriver() {
		if (ieDriver == null) {
			System.setProperty("webdriver.ie.driver", Config.getInstance()
					.getIEDriverPath());
			ieDriver = new InternetExplorerDriver();
			ieDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			ieDriver.navigate().to(INIT_URL);
			ieDriver.manage().window().maximize();
		}
		return ieDriver;
	}

	synchronized public ChromeDriver getChoromeDriver() {
		if (chromeDriver == null) {

			System.setProperty("webdriver.ie.driver", Config.getInstance()
					.getChromeDriverPath());
			
			ChromeOptions opt = new ChromeOptions();
	        opt.addArguments("--disable-notifications");
	        opt.addArguments("use-fake-ui-for-media-stream");
	        chromeDriver = new ChromeDriver(opt);
			chromeDriver.manage().timeouts()
					.implicitlyWait(10, TimeUnit.SECONDS);
			chromeDriver.navigate().to(INIT_URL);
			chromeDriver.manage().window().maximize();
		}
		return chromeDriver;
	}
	
	synchronized public void releaseWebDriver() {
		webDriver.close();
		webDriver = null;
	}

	synchronized public void releaseFireFoxDriver() {
		fireFoxDriver.close();
		fireFoxDriver = null;
	}

	synchronized public void releaseIEDriver() {
		ieDriver.close();
		ieDriver = null;
	}
}